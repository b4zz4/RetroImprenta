#Retro Imprenta

Imprimí en `ASCII` en los tiempos del `HD`.
Esta pensado para imprimir en una hoja de 140 columnas por 35 linea, esto se puede variar con los parámetros `-w` (ancho) `-s` (espacio) `-h` (alto)  y `-t` (titulo)


## Ejemplo

### Imprimir con titulo y portada

<pre>./retroimprenta -p ejemplo/portada.txt -t "Atari Punk" ejemplo/ataripunk-ascii.txt | lp</pre>

### Imprimir sin portada

<pre>./retroimprenta  ejemplo/ataripunk-ascii.txt | lp</pre>

_Si `lp` no te anda podes usar ` > /dev/lp0` puede dar problemas con algunos símbolos._

## Portadas

### Portada a partir de una imagen

Se pueden generar portadas a partir de imágenes

<pre>convert portada.png jpg:- | jp2a --width=140 - > portada.txt</pre>

### ASCII art a partir de un texto

Crea una portada a partir de un texto

<pre>toilet Portada -f bigmono12.tlf > portada.txt</pre>

## Experiencia

Cansado de no tener como imprimir a bajo costo en el [Hacklab de Barracas](http://hackcoop.com.ar/) pensen en comprar 
unas cintas para imprimir en una impresora antigua. El resto de las impresoras modernas que tenemos tienen carturos 
con [DRM](http://www.defectivebydesign.org/). 

En mis primeros intentos de imprimir volantes para el [Flisol](http://www.flisol.info/FLISOL2013/Argentina/Lanus), 
tuve algunos problemas con [DSL](http://damnsmalllinux.org/), no pude configurar `cups` fueron un tanto complicados, 
más que nada por que quería hacerlo con poco tiempo y bien :P

Luego en el hacklab de barracas [fauno](http://github.com/fauno) encontró la instrucción `pr` y reescribió el código 
mucho más breve.

## Quehaceres

- generar portadas al vuelo
- conversores de texto a versiones ASCII
- fuentes para portadas
- ejemplos de [toilet](http://libcaca.zoy.org/toilet.html)
- imágenes y tipografías con [jp2a](http://jp2a.sf.net) y [Imagick](http://www.imagemagick.org/Usage/text/)
- convertir textos para ser impresos: `ps2ascii`, `pstotext`, `pdftotext` y etc. 
